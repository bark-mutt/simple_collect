var request = require('request');
var cheerio = require('cheerio');
var extract = require('extract');


// using by sigle request
function spider (url) {
  console.log(`get http request: ${url}`)
  return new Promise ((resolve) => {
    httpRequest(url).then( result => {
      if(result) resolve(extract(result))
    })
  })

  function httpRequest(url) {
    let httpRespond = null
    const options = {
      url: url,
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1)' +
                      'AppleWebKit/537.36 (KHTML, like Gecko)} '+
                      'Chrome/41.0.2228.0 Safari/537.36'
      },
      strictSSL: false
    }

    do {
      const result = request.get(url, options)
      if (!result.content) {
        // bad request
        return false
      } else {
        httpRespond = cheerio.load(result.content)
        if ( httpRespond('meta').attr('http-equiv') === 'REFRESH') {
          const refreshUrl = httpRespond('meta').attr('content').split('=')[1]
          url += url.lastIndexOf('/') !== 0 ? '/' : ''
          url = refreshUrl.indexOf('http') < 0 ? url + refreshUrl : ''
        } else {
          return httpRespond
        }

      }
    } while ( httpRespond('meta').attr('http-equiv') === 'REFRESH' )

  }

}
