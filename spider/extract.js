function extract($) {
  const obj = {}
  obj.company_name = $('TITLE').text() || null
  obj.company_overview = $('meta[name="description"]').attr('content') || null
  obj.tagline = $('meta[name="keywords"]').attr('content') || null
  obj.social_network = {}
  $('a').map( (i, elemt) => {
    let href = $(elemt).attr('href')
    if (href && href.indexOf('facebook') > 0) obj.social_network.facebook = href
    if (href && href.indexOf('instagram') > 0) obj.social_network.instagram = href
    if (href && href.indexOf('youtube') > 0) obj.social_network.youtube = href
    if (href && href.indexOf('twitter') > 0) obj.social_network.twitter = href
    // get logo from web index return
    // change to vendor api function
    // if ( href.match('index') || href.match('/') ) {
    //   link.children.map((element) => {
    //     if (element.name === 'img' && typeof element.attribs.src !== 'undefined') {
    //       obj.logo_img = element.attribs.src
    //     }
    //   })
    //   if (obj.logo_img.indexOf('://') < 0) {
    //     obj.logo_img = url + obj.logo_img
    //   }
    // }
  })

  return obj
}

exports.extract = extract;
