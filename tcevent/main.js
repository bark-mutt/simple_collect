const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
const fs = require('fs')
const csv = require('fast-csv')
let csvDatas = []
let Tasks = []

function createWindow (campany) {
  let mainWindow = new BrowserWindow({width: 800, height: 600})
  mainWindow.loadURL(campany[2])

  mainWindow.webContents.on('dom-ready', function () {
    mainWindow.webContents.savePage('~/tmp/'+campany[0]+'.html', 'HTMLComplete', (error) => {
      if (!error) console.log(campany[0])
      setTimeout(function(){ mainWindow.destroy() }, 3000)
    })
  })

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', function(){
  var url = "mongodb://localhost:27017/admin";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    db.collection("tcevent").find({sort: cname}, function(err, res) {
      if (err) throw err;
      console.log("Number of documents inserted: " + res.insertedCount);
      db.close();
    });
  });
  fs.createReadStream("tc_troble_info.csv")
  // fs.createReadStream("tcevent.csv")
    .pipe(csv())
    .on("data", function(data){
      // csvDatas.push(data)
      Tasks.push(createWindow(data))
    })
    .on("end", function(){
      console.log("load csv finished");
      new Promise.all(Tasks).then(()=>{console.log(`--end--`)})
    });
})


app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
