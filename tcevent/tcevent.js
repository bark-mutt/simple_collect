var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var cheerio = require('cheerio');

// fs.readFile('Startup_Alley_San_Francisco_17.htm', function (error, html) {
//   const $ = cheerio.load(html);
//   let tcBrandProfile = []
//
//   $('.tc_profileTitle').each(function(i, element){
//     tcBrandProfile[i] = {}
//   // console.log(element.children[0].data)
//     tcBrandProfile[i].company_name = element.children[0].data
//   })
//
//   $('.tc_profileEmail span a').each(function(i, element){
//     if(element.children[0] !== undefined)
//       tcBrandProfile[i].contact_email = element.children[0].data
//   })
//
//   $('.tc_readMore').each(function(i, element){
//     // console.log(element.children[0])
//     tcBrandProfile[i].crunchBaseRef = $(element).attr('href')
//   })
//
//   var url = "mongodb://localhost:27017/admin";
//   MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     db.collection("tcevent").insertMany(tcBrandProfile, function(err, res) {
//       if (err) throw err;
//       console.log("Number of documents inserted: " + res.insertedCount);
//       db.close();
//     });
//   });
// });

function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function(filename) {
      fs.readFile(dirname + filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content);
      });
    });
  });
}

var data = [];
var index = 0
readFiles('archive/tmp/', function(filename, content) {
  const $ = cheerio.load(content);

  $('#profile_header_heading').children('a').each(function(i ,element){
    data[index] = {}
    data[index].company_name = element.children[0].data
  })

  $('.entity-info-card-primary-image').each(function(i, element){
    data[index].company_logo = element.attribs.src
  })

  $('.description-ellipsis').children('p').each(function(i, element){
    data[index].company_overview = element.children[0].data
  })

  $('.definition-list .container').each(function(i, element){
    let key, value
    let obj = {}
    $(element).children()
      .each(function(idx, elem) {
        if( idx%2 === 0 ){ // element tag dt
          key = elem.children[0].data.split(":")[0]
        } else { // element tag dd
          let dd = elem.children[0]
          if(dd.name == 'a')
            value = (dd.children[0]) ? dd.children[0].data : dd.attribs.href
          else
            value = dd.data
          obj[key] = value
        }
      })
    data[index].definition = obj
    index++
  })
}, function(err) {
  // console.log(err)
  // throw err;
  // console.log(data)
  var url = "mongodb://localhost:27017/admin";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    db.collection("crunchBase_temp").insertMany(data, function(err, res) {
      if (err) throw err;
      console.log("Number of documents inserted: " + res.insertedCount);
      db.close();
    });
  });
});
